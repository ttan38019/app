package com.example.app.Fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import com.example.app.R
import com.example.app.Activity.DocumentViewerActivity
import com.example.app.Adapter.DocumentAdapter

class DocumentPickerFragment : Fragment() {
    private var documentUris: ArrayList<Uri>? = null
    private var adapter: DocumentAdapter? = null
    private var filteredDocumentUris: ArrayList<Uri>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_documentpicker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    REQUEST_STORAGE_PERMISSION
                )
            }
        }

        documentUris = loadDocumentUris()
        filteredDocumentUris = ArrayList(documentUris)

        val buttonPick = view.findViewById<ImageView>(R.id.button_pick)
        val listView = view.findViewById<ListView>(R.id.list_view)
        val searchView = view.findViewById<SearchView>(R.id.search_view)

        adapter = DocumentAdapter(requireContext(), filteredDocumentUris!!, this)
        listView.adapter = adapter

        buttonPick.setOnClickListener { v: View? -> pickDocument() }

        listView.onItemLongClickListener =
            AdapterView.OnItemLongClickListener { parent: AdapterView<*>?, v: View, position: Int, id: Long ->
                v.findViewById<View>(R.id.delete_icon).visibility = View.VISIBLE
                v.findViewById<View>(R.id.delete_icon).setOnClickListener { view1: View? ->
                    removeDocumentUri(
                        filteredDocumentUris!![position]
                    )
                }
                true
            }

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent: AdapterView<*>?, v: View?, position: Int, id: Long ->
                val documentUri = filteredDocumentUris!![position]
                Log.e("DocumentManagement", "Item clicked: $documentUri")
                val intent = Intent(requireContext(), DocumentViewerActivity::class.java)
                intent.data = documentUri
                startActivity(intent)
            }

        val btAll = view.findViewById<TextView>(R.id.button_all)
        val btImg = view.findViewById<TextView>(R.id.button_images)
        val btPdf = view.findViewById<TextView>(R.id.button_videos)
        val btWord = view.findViewById<TextView>(R.id.button_audio)
        val btExcel = view.findViewById<TextView>(R.id.button_documents)

        btAll.setOnClickListener {
            filterDocuments("all")
            setButtonBold(btAll, btImg, btPdf, btWord, btExcel)
        }
        btImg.setOnClickListener {
            filterDocuments("image")
            setButtonBold(btImg, btAll, btPdf, btWord, btExcel)
        }
        btPdf.setOnClickListener {
            filterDocuments("pdf")
            setButtonBold(btPdf, btAll, btImg, btWord, btExcel)
        }
        btWord.setOnClickListener {
            filterDocuments("word")
            setButtonBold(btWord, btAll, btImg, btPdf, btExcel)
        }
        btExcel.setOnClickListener {
            filterDocuments("excel")
            setButtonBold(btExcel, btAll, btImg, btPdf, btWord)
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    filterDocumentsByName(query)
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    filterDocumentsByName(newText)
                }
                return true
            }
        })
    }

    private fun pickDocument() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf(
            "image/jpeg",
            "image/png",
            "application/pdf",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        ))
        startActivityForResult(intent, PICK_DOCUMENT_REQUEST_CODE)
    }


    @SuppressLint("WrongConstant")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_DOCUMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val uri = data.data
                if (uri != null) {
                    try {
                        val takeFlags = data.flags and (Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                        requireActivity().contentResolver.takePersistableUriPermission(uri, takeFlags)
                    } catch (e: SecurityException) {
                        e.printStackTrace()
                    }
                    documentUris!!.add(uri)
                    saveDocumentUris()
                    filteredDocumentUris!!.add(uri)
                    adapter!!.notifyDataSetChanged()
                }
            }
        }
    }

    private fun saveDocumentUris() {
        val sharedPreferences = requireActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        val uriStrings: MutableSet<String> = HashSet()
        for (uri in documentUris!!) {
            uriStrings.add(uri.toString())
        }
        editor.putStringSet(KEY_DOCUMENT_URIS, uriStrings)
        editor.apply()
    }

    private fun loadDocumentUris(): ArrayList<Uri> {
        val sharedPreferences = requireActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val uriStrings = sharedPreferences.getStringSet(KEY_DOCUMENT_URIS, HashSet())
        val uris = ArrayList<Uri>()
        for (uriString in uriStrings!!) {
            uris.add(Uri.parse(uriString))
        }
        return uris
    }

    public fun removeDocumentUri(uri: Uri) {
        documentUris!!.remove(uri)
        saveDocumentUris()
        filteredDocumentUris!!.remove(uri)
        adapter!!.notifyDataSetChanged()
    }

    private fun filterDocuments(type: String) {
        filteredDocumentUris!!.clear()
        when (type) {
            "all" -> filteredDocumentUris!!.addAll(documentUris!!)
            "image" -> {
                for (uri in documentUris!!) {
                    if (isImageFile(uri)) filteredDocumentUris!!.add(uri)
                }
            }
            "pdf" -> {
                for (uri in documentUris!!) {
                    if (isPdfFile(uri)) filteredDocumentUris!!.add(uri)
                }
            }
            "word" -> {
                for (uri in documentUris!!) {
                    if (isWordFile(uri)) filteredDocumentUris!!.add(uri)
                }
            }
            "excel" -> {
                for (uri in documentUris!!) {
                    if (isExcelFile(uri)) filteredDocumentUris!!.add(uri)
                }
            }
        }
        adapter!!.notifyDataSetChanged()
    }

    private fun filterDocumentsByName(query: String) {
        filteredDocumentUris!!.clear()
        for (uri in documentUris!!) {
            val displayName = getDisplayName(uri)
            if (displayName.contains(query, true)) {
                filteredDocumentUris!!.add(uri)
            }
        }
        adapter!!.notifyDataSetChanged()
    }

    private fun getDisplayName(uri: Uri): String {
        var displayName = ""
        val cursor = requireContext().contentResolver.query(uri, null, null, null, null)
        cursor?.use {
            if (it.moveToFirst()) {
                val nameIndex = it.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME)
                displayName = it.getString(nameIndex)
            }
        }
        return displayName
    }


    private fun isImageFile(uri: Uri): Boolean {
        val mimeType = requireContext().contentResolver.getType(uri)
        return mimeType != null && mimeType.startsWith("image/")
    }

    private fun isPdfFile(uri: Uri): Boolean {
        val mimeType = requireContext().contentResolver.getType(uri)
        return mimeType == "application/pdf"
    }

    private fun isWordFile(uri: Uri): Boolean {
        val mimeType = requireContext().contentResolver.getType(uri)
        return mimeType == "application/msword" ||
                mimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    }

    private fun isExcelFile(uri: Uri): Boolean {
        val mimeType = requireContext().contentResolver.getType(uri)
        return mimeType == "application/vnd.ms-excel" ||
                mimeType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    }
    private fun setButtonBold(selectedButton: TextView, vararg otherButtons: TextView) {
        selectedButton.setTypeface(null, Typeface.BOLD)
        for (button in otherButtons) {
            button.setTypeface(null, Typeface.NORMAL)
        }
    }

    companion object {
        private const val PICK_DOCUMENT_REQUEST_CODE = 1
        private const val REQUEST_STORAGE_PERMISSION = 100
        private const val PREFS_NAME = "document_prefs"
        private const val KEY_DOCUMENT_URIS = "document_uris"
    }
}
