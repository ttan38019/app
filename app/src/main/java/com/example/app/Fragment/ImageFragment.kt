package com.example.app.Fragment

import android.app.Dialog
import android.app.RecoverableSecurityException
import android.content.ContentResolver
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.app.Activity.DetailImageActivity
import com.example.app.Adapter.filterAdapter.SharePhotoAdapter
import com.example.app.R
import com.example.app.data.OnSelectionModeChange
import com.example.app.data.SharedPreferenceManager
import com.example.app.data.SharedStoragePhoto
import com.example.app.databinding.FragmentImageBinding
import com.example.app.utilities.sdk29AndUp
import com.example.app.viewModel.PhotoViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.UUID


class ImageFragment : Fragment(), OnSelectionModeChange {

    private lateinit var binding: FragmentImageBinding
    private lateinit var viewModel: PhotoViewModel

    private lateinit var externalStoragePhotoAdapter: SharePhotoAdapter

    private var readPermissionGranted = false
    private var writePermissionGranted = false
    private lateinit var permissionsLauncher: ActivityResultLauncher<Array<String>>
    private lateinit var intentSenderLauncher: ActivityResultLauncher<IntentSenderRequest>

    private lateinit var contentObserver: ContentObserver

    private var deletedImageUri: Uri? = null
    private lateinit var loadingDialog: Dialog

    private var isSelectedMode = false
    private var isFavouriteTab = false

    private var photos = listOf<SharedStoragePhoto>()
    private var isExternalPhotosLoaded = false
    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.TakePicturePreview()) {
            lifecycleScope.launch {
                val isSavedSuccessfully = when {
                    writePermissionGranted -> it?.let { it ->
                        savePhotoToExternalStorage(
                            UUID.randomUUID().toString(),
                            it
                        )
                    }

                    else -> false
                }
                loadPhotosFromExternalStorageIntoRecyclerView()
                if (isSavedSuccessfully == true) {
                    Toast.makeText(
                        this@ImageFragment.context,
                        "Photo saved successfully",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        this@ImageFragment.context,
                        "Failed to save photo",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    private lateinit var sharedPreferenceManager: SharedPreferenceManager
    private lateinit var photoFavourite: MutableList<String>
    var favoriteImages: List<Uri> = emptyList()
    private lateinit var selectedImages: MutableList<String>
    private var contentResolver: ContentResolver? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        contentResolver = context?.contentResolver
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPreferenceManager = SharedPreferenceManager(context)
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_image, container, false)
        binding = FragmentImageBinding.inflate(layoutInflater)
        view.findViewById<View>(R.id.toolbar).visibility = View.VISIBLE
        val btnMenu: ImageView = view.findViewById(R.id.btnMenu)
        btnMenu.visibility = View.VISIBLE
        btnMenu.setOnClickListener { view ->
            val popupMenu = PopupMenu(context, view)
            try {
                val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                fieldMPopup.isAccessible = true
                val mPopup = fieldMPopup.get(popupMenu)
                mPopup.javaClass
                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.nav_list -> {
                        isFavouriteTab = false
                        closeSelectedMode()
                        externalStoragePhotoAdapter.updateFavouriteTab(isFavouriteTab)
                        loadPhotosFromExternalStorageIntoRecyclerView()
                        activity?.findViewById<TextView>(R.id.toolbar_title)?.text = "Image"
                        true
                    }

                    R.id.nav_cam -> {
                        isFavouriteTab = false
                        closeSelectedMode()
                        externalStoragePhotoAdapter.updateFavouriteTab(isFavouriteTab)
                        takePhoto.launch(null)
                        true
                    }

                    R.id.nav_favorite -> {
                        isFavouriteTab = true
                        closeSelectedMode()
                        externalStoragePhotoAdapter.updateFavouriteTab(isFavouriteTab)
                        loadFavouritePhotoIntoRecyclerView()
                        activity?.findViewById<TextView>(R.id.toolbar_title)?.text = "Favourite"
                        true
                    }

                    R.id.nav_add -> {
                        isFavouriteTab = false
                        closeSelectedMode()
                        externalStoragePhotoAdapter.updateFavouriteTab(isFavouriteTab)
                        Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        ).also { pickerIntent ->
                            pickerIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            startActivityForResult(pickerIntent, 100)
                        }
                        true
                    }
                    else -> false
                }
            }
            popupMenu.menuInflater.inflate(R.menu.menu_item, popupMenu.menu)
            popupMenu.show()
        }
        selectedImages = mutableListOf()
        sharedPreferenceManager = SharedPreferenceManager(requireContext())
        photoFavourite = sharedPreferenceManager.getFavoriteImages().toMutableList()
        viewModel = ViewModelProvider(this).get(PhotoViewModel::class.java)
        permissionsLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                readPermissionGranted = permissions[android.Manifest.permission.READ_MEDIA_IMAGES]
                    ?: readPermissionGranted
                if (readPermissionGranted) {
                    loadPhotosFromExternalStorageIntoRecyclerView()
                } else {
                    Toast.makeText(
                        this@ImageFragment.context,
                        "Can't read files without permission.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        updateOrRequestPermissions()
        loadingDialog = Dialog(requireContext()).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.dialog_loading)
            setCancelable(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        viewModel.isLoading.observe(viewLifecycleOwner, Observer { isLoading ->
            if (isLoading) {
                showLoadingDialog()
            } else {
                lifecycleScope.launch {
                    hideLoadingDialog()
                }
            }
        })
        view.findViewById<ImageView>(R.id.toolbar_back).visibility = View.GONE
        view.findViewById<ImageView>(R.id.toolbar_back).setOnClickListener {
            if (isSelectedMode) {
                closeSelectedMode()
                externalStoragePhotoAdapter.notifyDataSetChanged()
            }
        }

        view.findViewById<TextView>(R.id.toolbar_title).text = "Image"

        var rvPublicPhotos = view.findViewById<RecyclerView>(R.id.rvPublicPhotos)
        rvPublicPhotos.layoutManager = StaggeredGridLayoutManager(3, RecyclerView.VERTICAL)
        externalStoragePhotoAdapter = SharePhotoAdapter(
            onPhotoClick = { photo, position ->
                Log.e("CHECKCLICK", "CLICKED")
                if (!isSelectedMode) {
                    val allPhotos = photos.map { it.contentUri.toString() }
                    openDetailImageActivity(allPhotos, position)
                } else {
                    if (selectedImages.contains(photo.contentUri.toString())) {
                        selectedImages.remove(photo.contentUri.toString())
                    } else {
                        selectedImages.add(photo.contentUri.toString())
                        if (!isFavouriteTab){
                            favoriteImages = sharedPreferenceManager.getFavoriteImages().map { Uri.parse(it) }
                        }
                    }
                    if (selectedImages.size == photos.size) {
                        view.findViewById<RadioButton>(R.id.rbSelectAll).isChecked = true
                    } else {
                        view.findViewById<RadioButton>(R.id.rbSelectAll).isChecked = false
                    }
                    if(!isFavouriteTab){
                        if (isThereAnyNonFavouriteSelected()) {
                            activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.visibility = View.VISIBLE
                            activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.visibility = View.VISIBLE
                        } else {
                            activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.visibility = View.GONE
                            activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.visibility = View.GONE
                        }
                    }
                }
            },
            onPhotoLongClick = { photo ->
                if (!isSelectedMode){
                    isSelectedMode = true
                    selectedImages.add(photo.contentUri.toString())
                    view.findViewById<RadioButton>(R.id.rbSelectAll).visibility = View.VISIBLE
                    view.findViewById<ImageView>(R.id.toolbar_back).visibility = View.VISIBLE
                    view.findViewById<ImageView>(R.id.toolbar_back)
                        .setImageResource(R.drawable.ic_close)
                    showUnderToolBar()
                    externalStoragePhotoAdapter.notifyDataSetChanged()
                }
            },
            onHideLoadingDialog = { lifecycleScope.launch { hideLoadingDialog() } },
            isSelectedMode,
            onSelectionModeChange = this,
            sharedPreferenceManager = sharedPreferenceManager,
            isFavouriteTab = isFavouriteTab
        )
        initContentObserver()

        rvPublicPhotos.adapter = externalStoragePhotoAdapter

        intentSenderLauncher =
            registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
                if (it.resultCode == AppCompatActivity.RESULT_OK) {
                    if (android.os.Build.VERSION.SDK_INT == android.os.Build.VERSION_CODES.Q) {
                        lifecycleScope.launch {
                            deletePhotoFromExternalStorage(deletedImageUri ?: return@launch)
                        }
                    }
                    sharedPreferenceManager = SharedPreferenceManager(requireContext())
                    photoFavourite = sharedPreferenceManager.getFavoriteImages().toMutableList()
                    if (photoFavourite.contains(deletedImageUri.toString())) {
                        photoFavourite.remove(deletedImageUri.toString())
                        sharedPreferenceManager.saveFavoriteImages(photoFavourite)
                    }
                    loadPhotosFromExternalStorageIntoRecyclerView()
                    Toast.makeText(
                        this@ImageFragment.context,
                        "Photo deleted successfully",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        this@ImageFragment.context,
                        "Photo couldn't be deleted",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadPhotosFromExternalStorageIntoRecyclerView()
        setClickInSelectedMode()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == 100) {
            handlePickerResult(data)
        }
    }

    private fun handlePickerResult(data: Intent?) {
        val selectedImageUri = data?.data
        selectedImageUri?.let { imageUri ->
            val mimeType = contentResolver?.getType(imageUri)
            if (mimeType == "image/jpeg" || mimeType == "image/png" || mimeType == "image/jpg") {
                // Open the EditImageActivity
                Intent(context, DetailImageActivity::class.java).also { editImageIntent ->
                    editImageIntent.putExtra("imageUri", imageUri.toString())
                    startActivity(editImageIntent)
                }
            } else {
                // Show an error message
                Toast.makeText(
                    this.context,
                    "Only PNG and JPEG images are supported",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun initContentObserver() {
        contentObserver = object : ContentObserver(null) {
            override fun onChange(selfChange: Boolean) {
                if (readPermissionGranted) {
                    isExternalPhotosLoaded = false
                    loadPhotosFromExternalStorageIntoRecyclerView()
                }
            }
        }
        contentResolver?.registerContentObserver(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            true,
            contentObserver
        )
    }

    private suspend fun deletePhotoFromExternalStorage(photoUri: Uri) {
        withContext(Dispatchers.IO) {
            try {
                contentResolver?.delete(photoUri, null, null)
            } catch (e: SecurityException) {
                val intentSender = when {
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                        if (contentResolver != null) {
                            MediaStore.createDeleteRequest(
                                contentResolver!!,
                                listOf(photoUri)
                            ).intentSender
                        } else {
                            null
                        }
                    }

                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
                        val recoverableSecurityException = e as? RecoverableSecurityException
                        recoverableSecurityException?.userAction?.actionIntent?.intentSender
                    }

                    else -> null
                }
                intentSender?.let { sender ->
                    intentSenderLauncher.launch(
                        IntentSenderRequest.Builder(sender).build()
                    )
                }
            }
        }
    }

    private suspend fun loadPhotosFromExternalStorage(): List<SharedStoragePhoto> {
        return withContext(Dispatchers.IO) {
            val collection = sdk29AndUp {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
            } ?: MediaStore.Images.Media.EXTERNAL_CONTENT_URI

            val projection = arrayOf(
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.WIDTH,
                MediaStore.Images.Media.HEIGHT,
            )
            val photos = mutableListOf<SharedStoragePhoto>()

            contentResolver?.query(
                collection,
                projection,
                null,
                null,
                "${MediaStore.Images.Media.DATE_ADDED} DESC"
            )?.use { cursor ->
                val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
                val displayNameColumn =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                val widthColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.WIDTH)
                val heightColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.HEIGHT)

                while (cursor.moveToNext()) {
                    val id = cursor.getLong(idColumn)
                    val displayName = cursor.getString(displayNameColumn)
                    val width = cursor.getInt(widthColumn)
                    val height = cursor.getInt(heightColumn)
                    val contentUri = ContentUris.withAppendedId(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        id
                    )
                    photos.add(SharedStoragePhoto(id, displayName, width, height, contentUri))
                }
            }
            if(contentResolver == null){
                Log.d("PhotosSize", "ContentResolver is null")
            }
            Log.d("PhotosSize", "Photos: ${photos.size}")
            photos.toList()
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun updateOrRequestPermissions() {
        val hasReadPermission = ContextCompat.checkSelfPermission(
            requireContext(),
            android.Manifest.permission.READ_MEDIA_IMAGES
        ) == PackageManager.PERMISSION_GRANTED
        val hasWritePermission = ContextCompat.checkSelfPermission(
            requireContext(),
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        val minSdk29 = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q

        readPermissionGranted = hasReadPermission
        writePermissionGranted = hasWritePermission || minSdk29

        val permissionsToRequest = mutableListOf<String>()
        if (!writePermissionGranted) {
            permissionsToRequest.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (!readPermissionGranted) {
            permissionsToRequest.add(android.Manifest.permission.READ_MEDIA_IMAGES)
        }
        if (permissionsToRequest.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                context as AppCompatActivity,
                permissionsToRequest.toTypedArray(),
                100
            )
        }
    }

    private suspend fun savePhotoToExternalStorage(displayName: String, bmp: Bitmap): Boolean {
        return withContext(Dispatchers.IO) {
            val imageCollection = sdk29AndUp {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
            } ?: MediaStore.Images.Media.EXTERNAL_CONTENT_URI

            val contentValues = ContentValues().apply {
                put(MediaStore.Images.Media.DISPLAY_NAME, "$displayName.jpg")
                put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
                put(MediaStore.Images.Media.WIDTH, bmp.width)
                put(MediaStore.Images.Media.HEIGHT, bmp.height)
            }
            try {
                contentResolver?.insert(imageCollection, contentValues)?.also { uri ->
                    contentResolver?.openOutputStream(uri).use { outputStream ->
                        if (!outputStream?.let {
                                bmp.compress(
                                    Bitmap.CompressFormat.PNG,
                                    95,
                                    it
                                )
                            }!!) {
                            throw IOException("Couldn't save bitmap")
                        }
                    }
                } ?: throw IOException("Couldn't create MediaStore entry")
                true
            } catch (e: IOException) {
                e.printStackTrace()
                false
            }
        }
    }

    private fun setupExternalStorageRecyclerView() = binding.rvPublicPhotos.apply {
        adapter = externalStoragePhotoAdapter
        layoutManager = StaggeredGridLayoutManager(3, RecyclerView.VERTICAL)
        binding.rvPublicPhotos.setHasFixedSize(true)

        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                // Update the visible item positions
                val layoutManager = recyclerView.layoutManager as StaggeredGridLayoutManager
                val visibleItemPositions = IntArray(layoutManager.spanCount)
                layoutManager.findLastVisibleItemPositions(visibleItemPositions)
                (adapter as SharePhotoAdapter).setLastVisibleItemPosition(
                    visibleItemPositions.get(
                        2
                    )
                )
            }
        })
    }

    private fun loadPhotosFromExternalStorageIntoRecyclerView() {
        lifecycleScope.launch {
            photos = loadPhotosFromExternalStorage()
            externalStoragePhotoAdapter.submitList(photos)
            isExternalPhotosLoaded = true
            viewModel.setLoading(false)
        }
    }

    private fun loadFavouritePhotoIntoRecyclerView() {
        lifecycleScope.launch {
            photos = loadPhotosFromExternalStorage()
            favoriteImages = sharedPreferenceManager.getFavoriteImages().map { Uri.parse(it) }
            photos = photos.filter { favoriteImages.contains(it.contentUri) }
            externalStoragePhotoAdapter.submitList(photos)
            externalStoragePhotoAdapter.notifyDataSetChanged()
            isExternalPhotosLoaded = true
            viewModel.setLoading(false)

            Log.d("FavoritePhotosInFragment", "Photos: $photos")
        }
        Log.d("FavoritePhotosInFragment", "Photos: $photos")
    }

    private fun loadPhotosFromExternalStorageIntoRecyclerView(listPhotos: List<SharedStoragePhoto>) {
        if (!isExternalPhotosLoaded) {
            viewModel.setLoading(true)
            lifecycleScope.launch {
                photos = listPhotos
                externalStoragePhotoAdapter.submitList(photos)
                externalStoragePhotoAdapter.notifyDataSetChanged()
                isExternalPhotosLoaded = true
                viewModel.setLoading(false)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        contentResolver?.unregisterContentObserver(contentObserver)
    }

    private fun openDetailImageActivity(images: List<String>, position: Int) {
        val intent = Intent(this.context, DetailImageActivity::class.java)
        intent.putStringArrayListExtra("images", ArrayList(images))
        intent.putExtra("position", position)
        startActivity(intent)
    }

    private fun showLoadingDialog() {
        loadingDialog.show()
    }

    private suspend fun hideLoadingDialog() {
        delay(100L)
        loadingDialog.dismiss()
    }

    override fun onSelectionModeChange(isInSelectionMode: Boolean) {
        this.isSelectedMode = isInSelectionMode
    }

    fun setClickInSelectedMode() {
        activity?.findViewById<ImageView>(R.id.ubtnDelete)?.setOnClickListener {
            lifecycleScope.launch {
                Log.e("CHECKCLICK", "CLICKED in selected mode delete")
                selectedImages.forEach {
                    val uri = Uri.parse(it)
                    if (photoFavourite.contains(uri.toString())) {
                        photoFavourite.remove(uri.toString())
                        sharedPreferenceManager.saveFavoriteImages(photoFavourite)
                    }
                    deletePhotoFromExternalStorage(uri)
                }
                closeSelectedMode()
                loadPhotosFromExternalStorageIntoRecyclerView()
                externalStoragePhotoAdapter.notifyDataSetChanged()
            }
        }
        activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.setOnClickListener {
            lifecycleScope.launch {
                Log.e("CHECKCLICK", "CLICKED in selected mode favorite")
                selectedImages.forEach {
                    val uri = Uri.parse(it)
                    if (!isFavouriteTab) {
                        if (!photoFavourite.contains(uri.toString())) {
                            photoFavourite.add(uri.toString())
                        }
                    }else {
                        photoFavourite.remove(uri.toString())
                        loadFavouritePhotoIntoRecyclerView()
                    }
                }
                sharedPreferenceManager.saveFavoriteImages(photoFavourite)
                closeSelectedMode()
                externalStoragePhotoAdapter.notifyDataSetChanged()
            }
        }
        activity?.findViewById<ImageView>(R.id.ubtnShare)?.setOnClickListener {
            lifecycleScope.launch {
                Log.e("CHECKCLICK", "CLICKED in selected mode share")
                selectedImages.forEach {
                    val uri = Uri.parse(it)
                    val shareIntent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_STREAM, uri)
                        type = "image/jpeg"
                    }
                    startActivity(Intent.createChooser(shareIntent, "Share image"))
                }
                closeSelectedMode()
                externalStoragePhotoAdapter.notifyDataSetChanged()
            }
        }
        activity?.findViewById<RadioButton>(R.id.rbSelectAll)?.setOnClickListener {
            if (selectedImages.size == photos.size) {
                externalStoragePhotoAdapter.clearSelectedPositions()
                selectedImages.clear()
                view?.findViewById<RadioButton>(R.id.rbSelectAll)?.isChecked = false
            } else {
                selectedImages.clear()
                externalStoragePhotoAdapter.selectAll()
                photos.forEach {
                    selectedImages.add(it.contentUri.toString())
                }
                view?.findViewById<RadioButton>(R.id.rbSelectAll)?.isChecked = true
            }
            if (isThereAnyNonFavouriteSelected()) {
                activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.visibility = View.VISIBLE
                activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.visibility = View.VISIBLE
            } else {
                activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.visibility = View.GONE
                activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.visibility = View.GONE
            }
            externalStoragePhotoAdapter.notifyDataSetChanged()
        }
    }

    private fun showUnderToolBar() {
        activity?.findViewById<LinearLayout>(R.id.underToolbar)?.visibility = View.VISIBLE
        if (isFavouriteTab) {
            activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.setColorFilter(resources.getColor(R.color.red_500))
            activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.text = "Unfavorite"
            activity?.findViewById<ImageView>(R.id.ivImageFavorite)?.visibility = View.GONE
            activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.visibility = View.VISIBLE
            activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.visibility = View.VISIBLE
        } else {
            activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.setColorFilter(resources.getColor(R.color.gray_400))
            activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.text = "Favourite"
            activity?.findViewById<ImageView>(R.id.ubtnFavorite)?.visibility = View.GONE
            activity?.findViewById<TextView>(R.id.tvUnderToolFavourite)?.visibility = View.GONE
        }
    }

    private fun closeSelectedMode() {
        isSelectedMode = false
        selectedImages.clear()
        view?.findViewById<RadioButton>(R.id.rbSelectAll)?.isChecked = false
        view?.findViewById<RadioButton>(R.id.rbSelectAll)?.visibility = View.GONE
        view?.findViewById<ImageView>(R.id.toolbar_back)?.visibility = View.GONE
        externalStoragePhotoAdapter.updateSelectionMode(isSelectedMode)
        view?.findViewById<LinearLayout>(R.id.underToolbar)?.visibility = View.GONE
    }

    private fun isThereAnyNonFavouriteSelected(): Boolean {
        return selectedImages.any { !photoFavourite.contains(it) }
    }
}