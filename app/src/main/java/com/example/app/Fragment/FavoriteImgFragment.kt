package com.example.app.Fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.app.Activity.DetailImageActivity
import com.example.app.Adapter.FavoriteImagesAdapter
import com.example.app.R
import com.example.app.data.SharedPreferenceManager
import kotlinx.coroutines.launch

class FavoriteImgFragment : Fragment() {
    private lateinit var adapter: FavoriteImagesAdapter
    private lateinit var sharedPreferenceManager: SharedPreferenceManager
    var favoriteImages: List<Uri> = emptyList()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPreferenceManager = SharedPreferenceManager(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sharedPreferenceManager.getFavoriteImagesLiveData().observe(viewLifecycleOwner) { favoriteImages ->
            loadFavoriteImgIntoRecyclerView()
        }
        var view = inflater.inflate(R.layout.fragment_favorite_img, container, false)
        val favoriteImages = sharedPreferenceManager.getFavoriteImages().map { Uri.parse(it) }
        val favoriteImagesRecyclerView = view.findViewById<RecyclerView>(R.id.favoriteImagesRecyclerView)
        setupRecyclerView()
        favoriteImagesRecyclerView.layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        adapter = FavoriteImagesAdapter(favoriteImages, requireContext()) { uri, position ->
            val imageStrings = favoriteImages.map { it.toString() }
            openDetailImageActivity(imageStrings, position)
        }
        favoriteImagesRecyclerView.adapter = adapter
        loadFavoriteImgIntoRecyclerView()
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedPreferenceManager.favoriteImagesLiveData.observe(viewLifecycleOwner) { favoriteImages ->
            adapter.submitList(favoriteImages.map { Uri.parse(it) })
        }
    }

    private fun setupRecyclerView() = view?.findViewById<RecyclerView>(R.id.favoriteImagesRecyclerView)?.apply {
        layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        adapter = this@FavoriteImgFragment.adapter
    }

    private fun loadFavoriteImgIntoRecyclerView(){
        lifecycleScope.launch {
            favoriteImages = sharedPreferenceManager.getFavoriteImages().map { Uri.parse(it) }
            adapter.submitList(favoriteImages)
        }
    }

    private fun openDetailImageActivity(images: List<String>, position: Int) {
        val intent = Intent(requireContext(), DetailImageActivity::class.java)
        intent.putStringArrayListExtra("images", ArrayList(images)) // Pass the list of URIs as strings
        intent.putExtra("position", position) // Pass the position of the clicked image
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        loadFavoriteImgIntoRecyclerView()
    }
}
