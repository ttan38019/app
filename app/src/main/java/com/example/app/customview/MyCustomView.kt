package com.example.demoservices.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.example.app.R

class MyCustomView : View {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val defaultColor = Color.GRAY
    private val defaultTextColor = Color.WHITE

    var textToDraw: String = "A"
        set(value) {
            field = value
            invalidate()
        }

    private var backgroundColor: Int = defaultColor
    private var textColor: Int = defaultTextColor


    init {
        paint.color = defaultColor
        paint.style = Paint.Style.FILL
        paint.textAlign = Paint.Align.CENTER
    }

    constructor(context: Context?) : super(context) {}

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        val typeArray = context?.obtainStyledAttributes(attrs, R.styleable.MyCustomView)

        val color = typeArray?.getColor(R.styleable.MyCustomView_exampleColor, defaultColor)

        if (color != null) {
            paint.color = color
        }

        typeArray?.recycle()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            measureSize(widthMeasureSpec),
            measureSize(heightMeasureSpec)
        )
    }

    fun measureSize(measureSpec: Int): Int {
        val mode = MeasureSpec.getMode(measureSpec)
        val size = MeasureSpec.getSize(measureSpec)

        return when (mode) {
            MeasureSpec.EXACTLY -> {
                size
            }

            MeasureSpec.AT_MOST -> {
                size.coerceAtMost(100)
            }

            MeasureSpec.UNSPECIFIED -> {
                size.coerceAtMost(100)
            }

            else -> {
                0
            }
        }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val radius = width / 2f
        val centerX = width / 2f
        val centerY = height / 2f

        // Draw the circle
        paint.color = backgroundColor
        canvas.drawCircle(centerX, centerY, radius, paint)

        // Set text properties
        val textSize = radius * 0.8f // Adjust text size as needed
        paint.textSize = textSize
        paint.color = textColor // Sử dụng màu văn bản đã lưu

        // Draw the text inside the circle
        canvas.drawText(textToDraw, centerX, centerY + textSize / 2 - 10, paint)
    }

    // Function to update the text value
    fun updateText(newText: String) {
        textToDraw = newText
    }

    fun changeColor(backgroundColor: Int) {
        this.backgroundColor = backgroundColor
        invalidate()
    }
}