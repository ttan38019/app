package com.example.app.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PhotoViewModel : ViewModel() {
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> get() = _isLoading

    fun setLoading(loading: Boolean) {
        _isLoading.value = loading
    }

    private val _photosLoaded = MutableLiveData<Int>()
    val photosLoaded: LiveData<Int> get() = _photosLoaded

    fun incrementPhotosLoaded() {
        _photosLoaded.value = (_photosLoaded.value ?: 0) + 1
    }

    fun resetPhotosLoaded() {
        _photosLoaded.value = 0
    }
}