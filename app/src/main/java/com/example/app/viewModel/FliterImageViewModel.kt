package com.example.app.viewModel

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app.data.ImageFilter
import com.example.app.utilities.Coroutines
import com.example.demoservices.repository.FilterImageRepository

class FliterImageViewModel(private val fliterImageRepository: FilterImageRepository) : ViewModel() {

    private val imagePreviewDataStage = MutableLiveData<ImagePreviewDataStage>()
    val imagePreviewUiStage: MutableLiveData<ImagePreviewDataStage> get() = imagePreviewDataStage

    fun prepareImagePreview(imageUri: Uri) {
        Coroutines.io {
            runCatching {
                emitImagePreviewUiStage(isLoading = true)
                fliterImageRepository.prepareImagePreview(imageUri)
            }.onSuccess { bitmap ->
                if (bitmap != null) {
                    emitImagePreviewUiStage(bitmap = bitmap)
                } else {
                    emitImagePreviewUiStage(error = "Something went wrong")
                }
            }.onFailure {
                emitImagePreviewUiStage(error = it.message.toString())
            }
        }
    }

    private fun emitImagePreviewUiStage(
        isLoading: Boolean = false,
        bitmap: Bitmap? = null,
        error: String? = null
    ) {
        val dataStage = ImagePreviewDataStage(isLoading, bitmap, error)
        imagePreviewDataStage.postValue(dataStage)
    }

    data class ImagePreviewDataStage(
        val isLoading: Boolean,
        val bitmap: Bitmap?,
        val error: String?
    )

    private val imageFilterDataStage = MutableLiveData<ImageFilterDataStag>()
    val imageFilterUiStage: MutableLiveData<ImageFilterDataStag> get() = imageFilterDataStage

    fun loadImageFilters(originalImage: Bitmap) {
        Coroutines.io {
            runCatching {
                emitImageFilterUiStage(isLoading = true)
                fliterImageRepository.getImageFilters(originalImage)
            }.onSuccess { imageFilters: List<ImageFilter> ->
                emitImageFilterUiStage(imageFilters = imageFilters)
            }.onFailure {
                emitImageFilterUiStage(error = it.message.toString())
            }
        }
    }

    private fun getPrivewImage(originalImage: Bitmap): Bitmap {
        return kotlin.runCatching {
            val previewWith = 150
            val previewHeight =
                (originalImage.height.toFloat() / originalImage.width.toFloat() * previewWith)
            Bitmap.createScaledBitmap(originalImage, previewWith, previewHeight.toInt(), false)
        }.getOrDefault(originalImage)
    }

    private fun emitImageFilterUiStage(
        isLoading: Boolean = false,
        imageFilters: List<ImageFilter>? = null,
        error: String? = null
    ) {
        val dataStage = ImageFilterDataStag(isLoading, imageFilters, error)
        imageFilterDataStage.postValue(dataStage)
    }

    data class ImageFilterDataStag(
        val isLoading: Boolean,
        val imageFilters: List<ImageFilter>?,
        val error: String?
    )
}