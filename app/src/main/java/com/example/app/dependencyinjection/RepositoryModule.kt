package com.example.app.dependencyinjection

import com.example.demoservices.repository.FilterImageRepository
import com.example.demoservices.repository.FilterImageRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    factory<FilterImageRepository> { FilterImageRepositoryImpl(androidContext()) }
}