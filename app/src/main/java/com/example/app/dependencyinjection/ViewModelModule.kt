package com.example.demoservices.dependencyinjection

import com.example.app.viewModel.FliterImageViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { FliterImageViewModel(fliterImageRepository = get()) }
}