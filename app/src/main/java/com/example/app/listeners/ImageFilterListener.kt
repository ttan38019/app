package com.example.demoservices.listeners

import com.example.app.data.ImageFilter


interface ImageFilterListener {
    fun onFilterSelect(imageFilter: ImageFilter)
}