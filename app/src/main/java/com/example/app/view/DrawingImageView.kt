package com.example.app.view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.constraintlayout.utils.widget.ImageFilterView

class DrawingImageView(context: Context, attrs: AttributeSet) : ImageFilterView(context, attrs) {

    private var drawPath: Path = Path()
    private var drawPaint: Paint = Paint()
    private var drawCanvas: Canvas? = null
    private var canvasBitmap: Bitmap? = null
    var isDrawingEnabled = false

    private var previousColor: Int = Color.BLACK
    private var previousStrokeWidth: Float = 10f
    private var eraserStrokeWidth: Float = 50f

    init {
        setupDrawing()
    }

    private fun setupDrawing() {
        drawPaint.color = Color.BLACK
        drawPaint.isAntiAlias = true
        drawPaint.strokeWidth = 10f
        drawPaint.style = Paint.Style.STROKE
        drawPaint.strokeJoin = Paint.Join.ROUND
        drawPaint.strokeCap = Paint.Cap.ROUND
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        drawCanvas = Canvas(canvasBitmap!!)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (canvasBitmap != null) {
            canvas.drawBitmap(canvasBitmap!!, 0f, 0f, null)
        }
        canvas.drawPath(drawPath, drawPaint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isDrawingEnabled) {
            return false
        }

        val touchX = event.x
        val touchY = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> drawPath.moveTo(touchX, touchY)
            MotionEvent.ACTION_MOVE -> drawPath.lineTo(touchX, touchY)
            MotionEvent.ACTION_UP -> {
                drawCanvas?.drawPath(drawPath, drawPaint)
                drawPath.reset()
            }

            else -> return false
        }

        invalidate()
        return true
    }

    fun setEraserSize(size: Float) {
        if (drawPaint.xfermode != null) { // Check if eraser is enabled
            drawPaint.strokeWidth = size
        }
    }

    fun setEraserOpacity(opacity: Int) {
        if (drawPaint.xfermode != null) { // Check if eraser is enabled
            val alpha = (opacity * 2.55).toInt() // convert from [0,100] to [0,255]
            drawPaint.alpha = alpha
        }
    }

    private var brushColor: Int = Color.BLACK
    private var brushSize: Float = 10f

    fun setBrushSize(size: Float) {
        brushSize = size
        if (drawPaint.xfermode == null) { // Check if eraser is not enabled
            drawPaint.strokeWidth = size
        }
    }

    fun setBrushColor(color: Int) {
        brushColor = color
        if (drawPaint.xfermode == null) { // Check if eraser is not enabled
            drawPaint.color = color
        }
    }

    fun enableEraser(enabled: Boolean) {
        if (enabled) {
            // Save the current color and stroke width
            previousColor = drawPaint.color
            previousStrokeWidth = drawPaint.strokeWidth

            drawPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
            drawPaint.strokeWidth = eraserStrokeWidth
        } else {
            drawPaint.xfermode = null
            drawPaint.color = brushColor
            drawPaint.strokeWidth = brushSize
        }
    }
}