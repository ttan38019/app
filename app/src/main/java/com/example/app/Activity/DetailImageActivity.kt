package com.example.app.Activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.RecoverableSecurityException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.viewpager.widget.ViewPager
import com.example.app.Adapter.ImagePagerAdapter
import com.example.app.R
import com.example.app.data.SharedPreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class DetailImageActivity : AppCompatActivity() {
    private lateinit var viewPager: ViewPager
    private var images: ArrayList<String> = ArrayList()
    private var position: Int = 0
    private lateinit var intentSenderLauncher: ActivityResultLauncher<IntentSenderRequest>
    private var deletedImageUri: Uri? = null
    private lateinit var sharedPreferenceManager: SharedPreferenceManager
    private lateinit var favoriteImages: MutableList<String>

    @SuppressLint("MissingInflatedId", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_image)
        supportActionBar?.hide()
        setupUnderToolbar()
        sharedPreferenceManager = SharedPreferenceManager(this)
        favoriteImages = sharedPreferenceManager.getFavoriteImages().toMutableList()
        viewPager = findViewById(R.id.viewPager)
        findViewById<ImageView>(R.id.toolbar_back).setOnClickListener {
            onBackPressed()
        }
        val extra = intent.getStringExtra("imageUri")
        if (extra != null) {
            images.add(extra)
        } else {
            images = intent.getStringArrayListExtra("images")!!
            position = intent.getIntExtra("position", 0)
        }
        val favoriteButton = findViewById<ImageView>(R.id.ubtnFavorite)
        Log.e("image", images[position].toString())
        val currentImageUri = Uri.parse(images[position]).toString()
        Log.e("image", currentImageUri)
        if (favoriteImages.contains(currentImageUri)) {
            favoriteButton.setColorFilter(resources.getColor(R.color.red_500))
        } else {
            favoriteButton.setColorFilter(resources.getColor(R.color.gray_400))
        }
        findViewById<ImageView>(R.id.btnEdit).visibility = ImageView.VISIBLE
        val adapter = ImagePagerAdapter(images)
        viewPager.adapter = adapter
        viewPager.currentItem = position
        updateToolbarTitle(viewPager.currentItem)
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                updateToolbarTitle(position)
                val favoriteButton = findViewById<ImageView>(R.id.ubtnFavorite)
                val currentImageUri = Uri.parse(images[position]).toString()
                if (favoriteImages.contains(currentImageUri)) {
                    favoriteButton.setColorFilter(resources.getColor(R.color.red_500))
                } else {
                    favoriteButton.setColorFilter(resources.getColor(R.color.gray_400))
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
        findViewById<ImageView>(R.id.ubtnShare).setOnClickListener {
            val shareIntent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_STREAM, Uri.parse(images[viewPager.currentItem]))
                type = "image/*"
            }
            startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.send_to)))
        }
        findViewById<ImageView>(R.id.btnEdit).setOnClickListener {
            val imageUri = Uri.parse(images[viewPager.currentItem])
            Intent(applicationContext, EditImageActivity::class.java).also { editImageIntent ->
                editImageIntent.putExtra("imageUri", imageUri.toString())
                startActivity(editImageIntent)
            }
        }
        findViewById<ImageView>(R.id.ubtnDelete).setOnClickListener {
            lifecycleScope.launch {
                val currentImageUri = Uri.parse(images[viewPager.currentItem])
                if (isInInternalStorage(currentImageUri)) {
                    if (favoriteImages.contains(currentImageUri.toString())) {
                        favoriteImages.remove(currentImageUri.toString())
                        sharedPreferenceManager.saveFavoriteImages(favoriteImages)
                    }
                } else {
                    deletePhotoFromExternalStorage(currentImageUri)
                    deletedImageUri = currentImageUri
                }
            }
        }
        findViewById<ImageView>(R.id.ubtnFavorite).setOnClickListener {
            val currentImageUri = Uri.parse(images[viewPager.currentItem])
            if (favoriteImages.contains(currentImageUri.toString())) {
                favoriteImages.remove(currentImageUri.toString())
                findViewById<ImageView>(R.id.ubtnFavorite).setColorFilter(resources.getColor(R.color.gray_400))
                Toast.makeText(this, "Removed from favorites", Toast.LENGTH_SHORT).show()
            } else {
                favoriteImages.add(currentImageUri.toString())
                findViewById<ImageView>(R.id.ubtnFavorite).setColorFilter(resources.getColor(R.color.red_500))
                Toast.makeText(this, "Added to favorites", Toast.LENGTH_SHORT).show()
            }
            sharedPreferenceManager.saveFavoriteImages(favoriteImages)
        }
        intentSenderLauncher =
            registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
                if (it.resultCode == RESULT_OK) {
                    if (android.os.Build.VERSION.SDK_INT == android.os.Build.VERSION_CODES.Q) {
                        lifecycleScope.launch {
                            deletePhotoFromExternalStorage(deletedImageUri ?: return@launch)
                        }
                    }
                    sharedPreferenceManager = SharedPreferenceManager(this@DetailImageActivity)
                    favoriteImages = sharedPreferenceManager.getFavoriteImages().toMutableList()
                    if (favoriteImages.contains(deletedImageUri.toString())) {
                        favoriteImages.remove(deletedImageUri.toString())
                        sharedPreferenceManager.saveFavoriteImages(favoriteImages)
                    }
                    onImageDeleted(deletedImageUri ?: return@registerForActivityResult)
                } else {
                    showErrorToast()
                }
            }
    }

    private fun onImageDeleted(uri: Uri) {
        val currentPosition = viewPager.currentItem
        images.removeAt(currentPosition)
        viewPager.adapter?.notifyDataSetChanged()
        Toast.makeText(this@DetailImageActivity, "Photo deleted successfully", Toast.LENGTH_SHORT)
            .show()
        val intent = Intent(this, DetailImageActivity::class.java)
        intent.putStringArrayListExtra(
            "images",
            ArrayList(images)
        ) // Pass the list of URIs as strings
        intent.putExtra("position", currentPosition) // Pass the position of the clicked image

        if (images.isNotEmpty()) {
            val newPosition =
                if (currentPosition >= images.size) images.size - 1 else currentPosition
            viewPager.currentItem = newPosition
            updateToolbarTitle(newPosition)
            setResult(Activity.RESULT_OK)
            finish()
            startActivity(intent)
        } else {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun showErrorToast() {
        Toast.makeText(this@DetailImageActivity, "Photo couldn't be deleted", Toast.LENGTH_SHORT)
            .show()
    }

    private fun updateToolbarTitle(position: Int) {
        val currentImageUri = Uri.parse(images[position])

        val projection = arrayOf(MediaStore.Images.Media.DATE_ADDED)

        try {
            contentResolver.query(currentImageUri, projection, null, null, null)?.use { cursor ->
                if (cursor.moveToFirst()) {
                    val dateAddedColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_ADDED)
                    val dateAdded = cursor.getLong(dateAddedColumnIndex)

                    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val date = Date(dateAdded * 1000)
                    val formattedDate = dateFormat.format(date)

                    findViewById<TextView>(R.id.toolbar_title).text = formattedDate
                } else {
                    Log.e("DetailImageActivity", "Cursor is empty or does not contain DATE_ADDED column")
                    findViewById<TextView>(R.id.toolbar_title).text = "Unknown Date"
                }
            }
        } catch (e: IllegalArgumentException) {
            Log.e("DetailImageActivity", "IllegalArgumentException: ${e.message}")
            findViewById<TextView>(R.id.toolbar_title).text = "Unknown Date"
        }
    }


    private suspend fun deletePhotoFromExternalStorage(photoUri: Uri) {
        withContext(Dispatchers.IO) {
            try {
                contentResolver.delete(photoUri, null, null)
            } catch (e: SecurityException) {
                val intentSender = when {
                    android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R -> {
                        MediaStore.createDeleteRequest(
                            contentResolver,
                            listOf(photoUri)
                        ).intentSender
                    }

                    android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q -> {
                        val recoverableSecurityException = e as? RecoverableSecurityException
                        recoverableSecurityException?.userAction?.actionIntent?.intentSender
                    }

                    else -> null
                }
                intentSender?.let { sender ->
                    intentSenderLauncher.launch(
                        IntentSenderRequest.Builder(sender).build()
                    )
                }
            }
        }
    }
    private fun isInInternalStorage(uri: Uri): Boolean {
        return "file" == uri.scheme
    }
    private fun getFilenameFromUri(uri: Uri): String {
        return uri.lastPathSegment ?: ""
    }
    private fun setupUnderToolbar(){
        findViewById<TextView>(R.id.tvUnderToolFavourite).visibility = TextView.GONE
        findViewById<TextView>(R.id.tvUnderToolShare).visibility = TextView.GONE
        findViewById<TextView>(R.id.tvUnderToolDelete).visibility = TextView.GONE
    }
}