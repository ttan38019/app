package com.example.app.Activity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.app.Fragment.DocumentPickerFragment
import com.example.app.Fragment.HomeFragment
import com.example.app.Fragment.ImageFragment

class ViewPagerAdapter(fm: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fm, behavior) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> HomeFragment()
            1 -> DocumentPickerFragment()
            2 -> ImageFragment()
            else -> HomeFragment()
        }
    }

    //tra ra so luong tab
    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = ""
        when (position) {
            0 -> title = "Home"
            1 -> title = "Document Picker"
            2 -> title = "Image Picker"
            3 -> title = "Setting"
        }
        return title
    }
}
