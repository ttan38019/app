package com.example.app.Activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import com.example.app.Adapter.filterAdapter.EditOptionAdapter
import com.example.app.Adapter.filterAdapter.ImageFilterAdapter
import com.example.app.R
import com.example.app.data.EditOption
import com.example.app.data.ImageFilter
import com.example.app.databinding.ActivityEditImageBinding
import com.example.app.utilities.displayToast
import com.example.app.utilities.sdk29AndUp
import com.example.app.utilities.show
import com.example.app.view.DrawingImageView
import com.example.app.viewModel.FliterImageViewModel
import com.example.demoservices.listeners.ImageFilterListener
import jp.co.cyberagent.android.gpuimage.GPUImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.androidx.viewmodel.ext.android.viewModel
import yuku.ambilwarna.AmbilWarnaDialog
import java.io.IOException
import java.util.UUID
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

class EditImageActivity : AppCompatActivity(), ImageFilterListener {

    private var dX = 0f
    private var dY = 0f

    private lateinit var binding: ActivityEditImageBinding
    private val viewModel: FliterImageViewModel by viewModel()
    private lateinit var gpuImage: GPUImage

    private lateinit var originalBitmap: Bitmap
    private val filterBitmaps = MutableLiveData<Bitmap>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditImageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        val editOptions = listOf(
            EditOption("Filter"),
            EditOption("Edit"),
            EditOption("Color"),
            EditOption("Brush"),
            EditOption("Emoji"),
            EditOption("Text"),
            EditOption("Crop"),
        )
        val rvList = listOf(
            binding.rvFilters,
            binding.rvTest,
            binding.rvColorRGB,
            binding.rvBrush,
            binding.rvEmoji,
            binding.rvText,
            binding.rvCrop
        )
        binding.rvEditOptions.adapter = EditOptionAdapter(editOptions, rvList).apply {
            selectedOption = editOptions.firstOrNull()
        }
        setListeners()
        setupObservers()
        setSeekBarChange()
        setColorRGB()
        setBrush()
        setText()
        prepareImagePreview()
    }

    private fun setupObservers() {
        binding.ivBack.visibility = View.VISIBLE
        binding.ivSaveFilter.visibility = View.VISIBLE
        viewModel.imagePreviewUiStage.observe(this) {
            val dataState = it ?: return@observe
            Log.e("EditImage", "DataState: $dataState")
            binding.previewProgress.visibility =
                if (dataState.isLoading) View.VISIBLE else View.GONE
            dataState.bitmap?.let { bitmap ->
                originalBitmap = bitmap
                filterBitmaps.value = bitmap
                with(originalBitmap) {
                    gpuImage.setImage(this)
                    binding.ImagePreview.show()
                    viewModel.loadImageFilters(this)
                }
            } ?: kotlin.run {
                dataState.error?.let { error ->
                    displayToast(error)
                }
            }
        }

        viewModel.imageFilterUiStage.observe(this) {
            val imageFilterdataState = it ?: return@observe
//            binding.filterProgress.visibility =
//                if (imageFilterdataState.isLoading) View.VISIBLE else View.GONE
            imageFilterdataState.imageFilters?.let { imageFilters ->
                ImageFilterAdapter(imageFilters, this).also { adapter ->
                    binding.rvFilters.adapter = adapter
                }
            } ?: kotlin.run {
                imageFilterdataState.error?.let { error ->
                    displayToast(error)
                }
            }
        }
        filterBitmaps.observe(this) { bitmap ->
            binding.ImagePreview.setImageBitmap(bitmap)
        }
    }

    private fun prepareImagePreview() {
        gpuImage = GPUImage(applicationContext)
        val uriString = intent.getStringExtra("imageUri")
        val uri = Uri.parse(uriString)
        uri?.let {
            viewModel.prepareImagePreview(it)
        }
    }

    override fun onFilterSelect(imageFilter: ImageFilter) {
        binding.ImagePreview.saturation = imageFilter.saturation
        binding.ImagePreview.brightness = imageFilter.brightness
        binding.ImagePreview.contrast = imageFilter.contrast
        binding.ImagePreview.warmth = imageFilter.warmth
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setListeners() {
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
        binding.ImagePreview.setOnLongClickListener {
            binding.ImagePreview.setImageBitmap(originalBitmap)
            return@setOnLongClickListener false
        }
        binding.ImagePreview.setOnClickListener {
            binding.ImagePreview.setImageBitmap(filterBitmaps.value)
        }

//        binding.ImagePreview.setOnTouchListener { v, event ->
//            when (event.actionMasked) {
//                MotionEvent.ACTION_DOWN -> {
//                    // Handle action down event
//                }
//                MotionEvent.ACTION_UP -> {
//                    // Handle action up event
//                    if (event.eventTime - event.downTime > ViewConfiguration.getLongPressTimeout()) {
//                        // It was a long press, so set the original bitmap
//                        binding.ImagePreview.setImageBitmap(originalBitmap)
//                    } else {
//                        // It was a click, so set the filtered bitmap
//                        binding.ImagePreview.setImageBitmap(filterBitmaps.value)
//                    }
//                }
//                else -> {
//                    // Other touch events like ACTION_MOVE and ACTION_CANCEL
//                }
//            }
//            true
//        }

        binding.ivSaveFilter.setOnClickListener {
            findViewById<ImageView>(R.id.imgPhotoEditorClose)?.visibility = View.GONE
            findViewById<ImageView>(R.id.imgPhotoEditorMove)?.visibility = View.GONE
            findViewById<ImageView>(R.id.imgPhotoEditorRotate)?.visibility = View.GONE
            findViewById<FrameLayout>(R.id.frmBorder)?.background = null
            binding.frmPhotoEditor.setBackgroundColor(Color.WHITE)
            getBitmapFromView(
                binding.ImagePreview,
                originalBitmap.width,
                originalBitmap.height
            )?.let { bitmap ->
                val canvas = Canvas(bitmap)
                binding.frmPhotoEditor.draw(canvas)
                lifecycleScope.launch {
                    val savedImageUri =
                        savePhotoToExternalStorage(UUID.randomUUID().toString(), bitmap)
                    if (savedImageUri != null) {
                        displayToast("Image saved successfully")
                        val intent = Intent(this@EditImageActivity, DetailImageActivity::class.java)
                        intent.putExtra("imageUri", savedImageUri.toString())
                        startActivity(intent)
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    } else {
                        displayToast("Failed to save image")
                    }
                }
            }
        }
    }

    private fun getBitmapFromView(view: View, width: Int, height: Int): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        view.layout(0, 0, width, height)
        return bitmap
    }

    private suspend fun savePhotoToExternalStorage(displayName: String, bmp: Bitmap): Uri? {
        return withContext(Dispatchers.IO) {
            val imageCollection = sdk29AndUp {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
            } ?: MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val contentValues = ContentValues().apply {
                put(MediaStore.Images.Media.DISPLAY_NAME, "$displayName.jpg")
                put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
                put(MediaStore.Images.Media.WIDTH, bmp.width)
                put(MediaStore.Images.Media.HEIGHT, bmp.height)
            }
            try {
                contentResolver.insert(imageCollection, contentValues)?.also { uri ->
                    contentResolver.openOutputStream(uri).use { outputStream ->
                        if (!outputStream?.let {
                                bmp.compress(
                                    Bitmap.CompressFormat.JPEG,
                                    95,
                                    it
                                )
                            }!!) {
                            throw IOException("Couldn't save bitmap")
                        }
                    }
                } ?: throw IOException("Couldn't create MediaStore entry")
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }
        }
    }

    private fun setSeekBarChange() {
        binding.sbSaturation.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.ImagePreview.saturation = progress * 0.01f
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        binding.sbBrightness.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.ImagePreview.brightness = progress * 0.01f
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        binding.sbContrast.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.ImagePreview.contrast = progress * 0.01f
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        binding.sbWarmth.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.ImagePreview.warmth = progress * 0.01f
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    private fun setColorRGB() {
        binding.sbRed.progress = 255
        binding.sbGreen.progress = 255
        binding.sbBlue.progress = 255
        binding.sbRed.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateImageColor()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        binding.sbGreen.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateImageColor()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        binding.sbBlue.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateImageColor()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    private fun updateImageColor() {
        val red = binding.sbRed.progress / 255f
        val green = binding.sbGreen.progress / 255f
        val blue = binding.sbBlue.progress / 255f
        val colorMatrix = ColorMatrix().apply {
            setScale(red, green, blue, 1f)
        }
        val filter = ColorMatrixColorFilter(colorMatrix)
        binding.ImagePreview.colorFilter = filter
    }

    private fun setBrush() {
        var isSelectBrush = false
        var isSelectEraser = false
        var brushColor = Color.BLACK
        // Get the DrawingImageView from the layout
        val drawingImageView = findViewById<DrawingImageView>(R.id.ImagePreview)
        binding.sbBrushSize.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                // Update the size of the brush
                drawingImageView.setBrushSize(progress.toFloat())
                drawingImageView.setEraserSize(progress.toFloat())
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        binding.sbBrushOpacity.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                // Update the opacity of the brush
                val alpha = (progress * 2.55).toInt()
                drawingImageView.setEraserOpacity(progress)
                drawingImageView.setBrushColor(
                    Color.argb(
                        alpha,
                        Color.red(brushColor),
                        Color.green(brushColor),
                        Color.blue(brushColor)
                    )
                )
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
        binding.ivBrushColor.setOnClickListener {
            val colorPicker =
                AmbilWarnaDialog(this, Color.BLACK, object : AmbilWarnaDialog.OnAmbilWarnaListener {
                    override fun onCancel(dialog: AmbilWarnaDialog) {
                        // do nothing
                    }
                    override fun onOk(dialog: AmbilWarnaDialog, color: Int) {
                        // Update the color of ivBrushColor
                        brushColor = color
                        drawingImageView.setBrushColor(color)
                        binding.ivBrushColor.setBackgroundColor(color)
                    }
                })
            colorPicker.show()
        }
        binding.ivBrush.setOnClickListener {
            binding.sbBrushSize.progress = 0
            binding.sbBrushOpacity.progress = 0
            binding.ivBrushColor.setBackgroundColor(Color.BLACK)
            if (isSelectBrush) {
                isSelectBrush = false
                isSelectEraser = false
                drawingImageView.isDrawingEnabled = false
                binding.ivBrush.setColorFilter(Color.BLACK)
            } else {
                isSelectBrush = true
                isSelectEraser = false
                drawingImageView.enableEraser(false)
                drawingImageView.isDrawingEnabled = true
                binding.ivBrushEraser.setColorFilter(Color.BLACK)
                binding.ivBrush.setColorFilter(Color.BLUE)
            }
        }
        binding.ivBrushEraser.setOnClickListener {
            binding.sbBrushSize.progress = 0
            binding.sbBrushOpacity.progress = 0
            binding.ivBrushColor.setBackgroundColor(Color.BLACK)
            if (isSelectEraser) {
                isSelectEraser = false
                isSelectBrush = false
                drawingImageView.isDrawingEnabled = false
                binding.ivBrushEraser.setColorFilter(Color.BLACK)
            } else {
                isSelectEraser = true
                isSelectBrush = false
                drawingImageView.isDrawingEnabled = true
                drawingImageView.enableEraser(true)
                binding.ivBrushEraser.setColorFilter(Color.BLUE)
                binding.ivBrush.setColorFilter(Color.BLACK)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setText() {
        var textColor = Color.BLACK
        val addedViews = mutableListOf<ViewInfo>()
        binding.ivTextColor.setOnClickListener {
            val colorPicker =
                AmbilWarnaDialog(this, Color.BLACK, object : AmbilWarnaDialog.OnAmbilWarnaListener {
                    override fun onCancel(dialog: AmbilWarnaDialog) {
                        // do nothing
                    }
                    override fun onOk(dialog: AmbilWarnaDialog, color: Int) {
                        binding.ivTextColor.setBackgroundColor(color)
                        textColor = color
                    }
                })
            colorPicker.show()
        }
        binding.ivAddText.setOnClickListener {
            // Inflate the text editor view
            val inflater = LayoutInflater.from(this)
            val textEditorView =
                inflater.inflate(R.layout.view_photo_editor_text, binding.frmPhotoEditor, false)
            // Set a unique id for the text editor view
            textEditorView.id = View.generateViewId()
            // Set the text for the TextView in the inflated view
            val textView = textEditorView.findViewById<TextView>(R.id.tvPhotoEditorText)
            textView.text = "Your text here"
            textView.setTextColor(textColor)
            // Add the text editor view to the root layout
            binding.frmPhotoEditor.addView(textEditorView)
            findViewById<View>(textEditorView.id).findViewById<EditText>(R.id.tvPhotoEditorText).isEnabled = true
            for (viewInfo in addedViews) {
                if (viewInfo.id != textEditorView.id) {
                    val otherView = findViewById<View>(viewInfo.id)
                    otherView.findViewById<ImageView>(R.id.imgPhotoEditorClose).visibility = View.GONE
                    otherView.findViewById<ImageView>(R.id.imgPhotoEditorMove).visibility = View.GONE
                    otherView.findViewById<ImageView>(R.id.imgPhotoEditorRotate).visibility = View.GONE
                    otherView.findViewById<FrameLayout>(R.id.frmBorder).background = null
                    otherView.findViewById<EditText>(R.id.tvPhotoEditorText).isEnabled = false
                }
            }
            val frmBorder = textEditorView.findViewById<FrameLayout>(R.id.frmBorder)
            val imgPhotoEditorClose =
                textEditorView.findViewById<ImageView>(R.id.imgPhotoEditorClose)
            val imgPhotoEditorMove = textEditorView.findViewById<ImageView>(R.id.imgPhotoEditorMove)
            val imgPhotoEditorRotate =
                textEditorView.findViewById<ImageView>(R.id.imgPhotoEditorRotate)
            val editText = findViewById<EditText>(R.id.tvPhotoEditorText)
            var downRawX: Float = 0f
            var downRawY: Float = 0f
            textEditorView.setOnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        // Remember where we started
                        dX = textEditorView.x - event.rawX
                        dY = textEditorView.y - event.rawY
                        downRawX = event.rawX
                        downRawY = event.rawY
                        // Hide imgPhotoEditorClose, imgPhotoEditorMove, and imgPhotoEditorRotate for all other textEditorViews
                        findViewById<View>(textEditorView.id).findViewById<EditText>(R.id.tvPhotoEditorText).isEnabled = true
                        for (viewInfo in addedViews) {
                            if (viewInfo.id != textEditorView.id) {
                                val otherView = findViewById<View>(viewInfo.id)
                                otherView.findViewById<ImageView>(R.id.imgPhotoEditorClose).visibility = View.GONE
                                otherView.findViewById<ImageView>(R.id.imgPhotoEditorMove).visibility = View.GONE
                                otherView.findViewById<ImageView>(R.id.imgPhotoEditorRotate).visibility = View.GONE
                                otherView.findViewById<FrameLayout>(R.id.frmBorder).background = null
                                otherView.findViewById<EditText>(R.id.tvPhotoEditorText).isEnabled = false
                            }
                        }
                    }

//                    MotionEvent.ACTION_MOVE -> {
//                        // Calculate the distance moved
//                        val newX = event.rawX + dX
//                        val newY = event.rawY + dY
//                        // Move the view
//                        textEditorView.animate()
//                            .x(newX)
//                            .y(newY)
//                            .setDuration(0)
//                            .start()
//                        // Update the position in the corresponding ViewInfo object
//                        val viewInfo = addedViews.find { it.id == textEditorView.id }
//                        if (viewInfo != null) {
//                            viewInfo.x = newX
//                            viewInfo.y = newY
//                        }
//                        // Log the new position
//                        Log.d("New Position", "x: $newX, y: $newY")
//                    }

                    MotionEvent.ACTION_UP -> {
                        // If the finger has not moved, it's a click
                        if (downRawX == event.rawX && downRawY == event.rawY) {
                            if (imgPhotoEditorClose.visibility == View.VISIBLE) {
                                imgPhotoEditorClose.visibility = View.GONE
                                imgPhotoEditorMove.visibility = View.GONE
                                imgPhotoEditorRotate.visibility = View.GONE
                                frmBorder.background = null
                                findViewById<View>(textEditorView.id).findViewById<EditText>(R.id.tvPhotoEditorText).isEnabled = false
                                binding.ivTextColor.setBackgroundColor(Color.BLACK)
                                textColor = Color.BLACK
                            } else {
                                imgPhotoEditorClose.visibility = View.VISIBLE
                                imgPhotoEditorMove.visibility = View.VISIBLE
                                imgPhotoEditorRotate.visibility = View.VISIBLE
                                frmBorder.background = getDrawable(R.drawable.rounded_border_tv)
                                findViewById<View>(textEditorView.id).findViewById<EditText>(R.id.tvPhotoEditorText).isEnabled = true
                                textColor = textView.currentTextColor
                                binding.ivTextColor.setBackgroundColor(textView.currentTextColor)
                                binding.ivTextColor.setOnClickListener {
                                    val colorPicker = AmbilWarnaDialog(
                                        this,
                                        Color.BLACK,
                                        object : AmbilWarnaDialog.OnAmbilWarnaListener {
                                            override fun onCancel(dialog: AmbilWarnaDialog) {
                                                // do nothing
                                            }
                                            override fun onOk(
                                                dialog: AmbilWarnaDialog,
                                                color: Int
                                            ) {
                                                binding.ivTextColor.setBackgroundColor(color)
                                                textView.setTextColor(color)
                                                textColor = color
                                            }
                                        })
                                    colorPicker.show()
                                }
                            }
                        }
                    }
                }
                true
            }

            // If this is not the first view, position it below the middle view
            if (addedViews.isNotEmpty()) {
                val middleViewInfo = addedViews[addedViews.size / 2]
                val middleView = findViewById<View>(middleViewInfo.id)
                if (middleView != null) {
                    // Calculate the new position of the view
                    val newY = middleView.y + middleView.height + 5.dpToPx(this)
                    // Set the new position of the view
                    textEditorView.translationX = middleView.x
                    textEditorView.translationY = newY
                    // Update the position in the ViewInfo object
                    addedViews.add(ViewInfo(textEditorView.id, textEditorView.x, textEditorView.y))
                } else {
                    // Handle the case where the middle view is not found
                    addedViews.add(ViewInfo(textEditorView.id, 0f, 0f))
                }
            } else {
                // If this is the first view, position it at the center of ImagePreview
                textEditorView.translationX = binding.ImagePreview.x
                textEditorView.translationY = binding.ImagePreview.y
                // Update the position in the ViewInfo object
                addedViews.add(ViewInfo(textEditorView.id, textEditorView.x, textEditorView.y))
            }
            addedViews.add(ViewInfo(textEditorView.id, textEditorView.x, textEditorView.y))
            // Handle the close button click event
            imgPhotoEditorClose.setOnClickListener {
                // Remove the text editor view when the close button is clicked
                binding.frmPhotoEditor.removeView(textEditorView)
                // Remove the text editor view from the list of added views
                val viewToRemove = addedViews.find { it.id == textEditorView.id }
                if (viewToRemove != null) {
                    addedViews.remove(viewToRemove)
                }
                binding.ivTextColor.setBackgroundColor(Color.BLACK)
                textColor = Color.BLACK
            }
            imgPhotoEditorMove.setOnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        // Remember where we started
                        dX = textEditorView.x - event.rawX
                        dY = textEditorView.y - event.rawY
                    }
                    MotionEvent.ACTION_MOVE -> {
                        // Calculate the distance moved
                        val newX = event.rawX + dX
                        val newY = event.rawY + dY
                        // Move the view
                        textEditorView.animate()
                            .x(newX)
                            .y(newY)
                            .setDuration(0)
                            .start()
                        // Update the position in the corresponding ViewInfo object
                        val viewInfo = addedViews.find { it.id == textEditorView.id }
                        if (viewInfo != null) {
                            viewInfo.x = newX
                            viewInfo.y = newY
                        }
                        // Log the new position
                        Log.d("New Position", "x: $newX, y: $newY")
                    }
                    else -> false
                }
                true
            }
            var initialRotation = 0f
            var newSize = 18f
            fun calculateAngle(x: Float, y: Float): Float {
                val centerX = textEditorView.x + textEditorView.width / 2
                val centerY = textEditorView.y + textEditorView.height / 2
                val dx = x - centerX
                val dy = y - centerY
                val zoomRatio = calculateVectorLength(dx, dy)/calculateVectorLength((centerX - textEditorView.x), (centerY - textEditorView.y))
                newSize = calculateNewSize(newSize, zoomRatio)
                val radians = atan2(dy.toDouble(), dx.toDouble())
                return Math.toDegrees(radians).toFloat()
            }
            imgPhotoEditorRotate.setOnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_MOVE -> {
                        val angle = calculateAngle(event.rawX, event.rawY)
                        textEditorView.rotation = initialRotation + angle
//                        textEditorView.findViewById<EditText>(R.id.tvPhotoEditorText).textSize = newSize
                    }
                    else -> false
                }
                true
            }
        }
        binding.frmPhotoEditor.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                // Hide imgPhotoEditorClose, imgPhotoEditorMove, and imgPhotoEditorRotate for all textEditorViews
                for (viewInfo in addedViews) {
                    val otherView = findViewById<View>(viewInfo.id)
                    otherView.findViewById<ImageView>(R.id.imgPhotoEditorClose).visibility = View.GONE
                    otherView.findViewById<ImageView>(R.id.imgPhotoEditorMove).visibility = View.GONE
                    otherView.findViewById<ImageView>(R.id.imgPhotoEditorRotate).visibility = View.GONE
                    otherView.findViewById<FrameLayout>(R.id.frmBorder).background = null
                    otherView.findViewById<EditText>(R.id.tvPhotoEditorText).isEnabled = false
                }
            }
            true
        }
    }

    private fun Int.dpToPx(context: Context): Float {
        return this * context.resources.displayMetrics.density
    }

    fun calculateVectorLength(x: Float, y: Float): Float {
        return sqrt(x.pow(2) + y.pow(2))
    }

    fun calculateNewSize(oldSize: Float, zoomRatio: Float): Float {
        return oldSize * zoomRatio
    }

    data class ViewInfo(val id: Int, var x: Float, var y: Float)
}