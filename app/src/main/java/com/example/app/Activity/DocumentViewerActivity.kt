package com.example.app.Activity

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.example.app.R
import com.github.barteksc.pdfviewer.PDFView
import java.io.IOException

class DocumentViewerActivity : AppCompatActivity() {
    private var mediaPlayer: MediaPlayer? = null
    private var isPlaying = false
    private lateinit var pdfView: PDFView
    private var documentUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_viewer)

        val videoView: VideoView = findViewById(R.id.videoViewDocument)
        pdfView = findViewById(R.id.pdfView)

        val buttonPlay: ImageView = findViewById(R.id.ic_play)
        val buttonPause: ImageView = findViewById(R.id.ic_pause)

        // Get the URI from the Intent
        documentUri = intent.data

        // Check the URI
        if (documentUri == null) {
            Log.e("DocumentViewerActivity", "Document Uri is null")
            finish()
            return
        }

        // Get the MIME type
        val mimeType = contentResolver.getType(documentUri!!)
        if (mimeType == null) {
            Log.e("DocumentViewerActivity", "MIME type is null")
            Toast.makeText(this, "Error displaying document", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        // Handle document display based on MIME type
        when {
            mimeType.startsWith("video/") -> {
                // Display video
                videoView.visibility = View.VISIBLE
                videoView.setVideoURI(documentUri)
                val mediaController = MediaController(this)
                mediaController.setAnchorView(videoView)
                videoView.setMediaController(mediaController)
                videoView.start()
            }
            mimeType == "application/pdf" -> {
                // Display PDF
                pdfView.visibility = View.VISIBLE
                pdfView.fromUri(documentUri).load()
            }
            mimeType.startsWith("audio/") -> {
                // Play audio
                buttonPlay.visibility = View.VISIBLE
                mediaPlayer = MediaPlayer().apply {
                    try {
                        setDataSource(this@DocumentViewerActivity, documentUri!!)
                        prepare()
                    } catch (e: IOException) {
                        Log.e("DocumentViewerActivity", "Error loading audio: ${e.message}")
                        Toast.makeText(this@DocumentViewerActivity, "Error loading audio", Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                        finish()
                        return
                    }
                    buttonPlay.setOnClickListener {
                        if (!isPlaying) {
                            start()
                            buttonPlay.visibility = View.GONE
                            buttonPause.visibility = View.VISIBLE
                        }
                        this@DocumentViewerActivity.isPlaying = true
                    }
                    buttonPause.setOnClickListener {
                        if (isPlaying) {
                            pause()
                            buttonPlay.visibility = View.VISIBLE
                            buttonPause.visibility = View.GONE
                        }
                        this@DocumentViewerActivity.isPlaying = false
                    }
                    setOnCompletionListener {
                        this@DocumentViewerActivity.isPlaying = false
                        buttonPlay.visibility = View.VISIBLE
                        buttonPause.visibility = View.GONE
                    }
                }
            }
            mimeType == "application/vnd.ms-excel" || mimeType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" -> {
                // Open Excel with external app
                val intent = Intent(Intent.ACTION_VIEW).apply {
                    setDataAndType(documentUri, mimeType)
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }
                try {
                    startActivity(intent)
                    finish() // Close the current activity if Excel app is launched
                } catch (e: Exception) {
                    Log.e("DocumentViewerActivity", "No application found to open Excel: ${e.message}")
                    Toast.makeText(this, "No application found to open Excel", Toast.LENGTH_SHORT).show()
                }
            }
            mimeType == "application/msword" || mimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" -> {
                // Open Word with external app
                val intent = Intent(Intent.ACTION_VIEW).apply {
                    setDataAndType(documentUri, mimeType)
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }
                try {
                    startActivity(intent)
                    finish() // Close the current activity if Word app is launched
                } catch (e: Exception) {
                    Log.e("DocumentViewerActivity", "No application found to open Word: ${e.message}")
                    Toast.makeText(this, "No application found to open Word", Toast.LENGTH_SHORT).show()
                }
            }
            else -> {
                // Unsupported document type
                Log.e("DocumentViewerActivity", "Unsupported document type: $mimeType")
                Toast.makeText(this, "Error displaying document", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer?.release()
        mediaPlayer = null
    }
}
