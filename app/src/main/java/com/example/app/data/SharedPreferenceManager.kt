package com.example.app.data

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class SharedPreferenceManager(context: Context) {
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("favorite_images", Context.MODE_PRIVATE)
    val favoriteImagesLiveData: MutableLiveData<List<String>> = MutableLiveData(getFavoriteImages())

    fun saveFavoriteImages(imageUris: List<String>) {
        val joinedUris = imageUris.joinToString(separator = ",")
        sharedPreferences.edit().putString("favorite_images", joinedUris).apply()
        favoriteImagesLiveData.value = imageUris
    }

    fun getFavoriteImagesLiveData(): LiveData<List<String>> {
        return favoriteImagesLiveData
    }

    fun getFavoriteImages(): List<String> {
        val joinedUris = sharedPreferences.getString("favorite_images", "") ?: ""
        return if (joinedUris.isEmpty()) {
            emptyList()
        } else {
            joinedUris.split(",")
        }
    }
}