package com.example.app.data

interface OnSelectionModeChange {
    fun onSelectionModeChange(isInSelectionMode: Boolean)
}