package com.example.app.data

data class EditOption(
    val name: String,
)