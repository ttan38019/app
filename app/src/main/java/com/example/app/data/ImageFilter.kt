package com.example.app.data

import android.graphics.Bitmap

data class ImageFilter(
    val name: String,
    val saturation: Float,
    val brightness: Float,
    val warmth: Float,
    val contrast: Float,
    var filterPreview: Bitmap,
    var isLoading: Boolean = true
)
