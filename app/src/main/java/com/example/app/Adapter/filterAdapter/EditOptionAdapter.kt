package com.example.app.Adapter.filterAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.app.R
import com.example.app.data.EditOption

class EditOptionAdapter(
    private val options: List<EditOption>,
    private val rvList: List<ViewGroup>
) : RecyclerView.Adapter<EditOptionAdapter.ViewHolder>() {

    var selectedOption: EditOption? = options.firstOrNull()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.tvEditOption)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_container_edit, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val option = options[position]
        holder.name.text = option.name

        val color = ContextCompat.getColor(
            holder.itemView.context,
            if (option == selectedOption) android.R.color.holo_blue_dark else android.R.color.black
        )

        holder.name.setTextColor(color)

        holder.itemView.setOnClickListener {
            selectedOption = option
            notifyDataSetChanged()
            for (i in rvList.indices) {
                if (i == position) {
                    rvList[i].visibility = View.VISIBLE
                } else {
                    rvList[i].visibility = View.GONE
                }
            }
        }
    }

    override fun getItemCount() = options.size
}