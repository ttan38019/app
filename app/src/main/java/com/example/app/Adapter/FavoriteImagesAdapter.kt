package com.example.app.Adapter

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.app.databinding.ItemPhotoBinding

class FavoriteImagesAdapter(
    private val favoriteImages: List<Uri>,
    requireContext: Context,
    private val onItemClicked: (Uri, Int) -> Unit
) : ListAdapter<Uri, FavoriteImagesAdapter.ViewHolder>(DiffCallback) {

    inner class ViewHolder(private val binding: ItemPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(uri: Uri) {
            Glide.with(binding.ivPhoto)
                .load(uri)
                .into(binding.ivPhoto)

            itemView.setOnClickListener { onItemClicked(uri, bindingAdapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.d("checkAdapter", "onCreateViewHolder called")
        val binding = ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("checkAdapter", "onBindViewHolder called for position $position")
        if (position < favoriteImages.size) {
            holder.bind(favoriteImages[position])
        } else {
            // Handle the case where the position is out of bounds of the list
        }
    }

    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<Uri>() {
            override fun areItemsTheSame(oldItem: Uri, newItem: Uri) = oldItem == newItem
            override fun areContentsTheSame(oldItem: Uri, newItem: Uri) = oldItem == newItem
        }
    }

    override fun getItemCount(): Int {
        val count = super.getItemCount()
        Log.d("checkAdapter", "getItemCount called, count: $count")
        return count
    }

    override fun submitList(list: List<Uri>?) {
        Log.d("checkAdapter", "submitList called with ${list?.size ?: 0} items")
        super.submitList(list)
    }
}