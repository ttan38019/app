package com.example.app.Adapter.filterAdapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.app.R
import com.example.app.data.OnSelectionModeChange
import com.example.app.data.SharedPreferenceManager
import com.example.app.data.SharedStoragePhoto
import com.example.app.databinding.ItemPhotoBinding

class SharePhotoAdapter(
    private val onPhotoClick: (SharedStoragePhoto, Int) -> Unit,
    private val onPhotoLongClick: (SharedStoragePhoto) -> Unit,
    private val onHideLoadingDialog: () -> Unit,
    private var isInSelectionMode: Boolean,
    private var isFavouriteTab: Boolean,
    private val onSelectionModeChange: OnSelectionModeChange,
    private var selectedPositions: MutableList<Int> = mutableListOf<Int>(),
    private var sharedPreferenceManager: SharedPreferenceManager,
    private var favoriteImages: MutableList<String> = mutableListOf<String>()
) : ListAdapter<SharedStoragePhoto, SharePhotoAdapter.PhotoViewHolder>(DiffCallback)  {
    private var visibleItemPositions: IntArray = IntArray(20) { it }
    private var lastVisibleItemPosition: Int = 0

    init {
        setHasStableIds(true)
    }

    inner class PhotoViewHolder(private val binding: ItemPhotoBinding) : RecyclerView.ViewHolder(binding.root) {
        private val rbSelect: RadioButton = itemView.findViewById(R.id.rbSelect)
        private val ivImageFavorite: ImageView = itemView.findViewById(R.id.ivImageFavorite)
        init {
            sharedPreferenceManager = SharedPreferenceManager(itemView.context)
            binding.root.setOnClickListener {
                val position = absoluteAdapterPosition
                Log.e("CHECKCLICK", "CLICKED in adapter")
                if (position != RecyclerView.NO_POSITION) {
                    getItem(position)?.let {
                        onPhotoClick(it, position)
                        if (isInSelectionMode) {
                            if (selectedPositions.contains(position)) {
                                selectedPositions.remove(position)
                            } else {
                                selectedPositions.add(position)
                            }
                            notifyDataSetChanged()
                        }
                    }
                }
            }
            binding.root.setOnLongClickListener {
                val position = absoluteAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    getItem(position)?.let { photo ->
                        onPhotoLongClick(photo)
                        if (!isInSelectionMode) {
                            selectedPositions.add(position)
                        }
                        isInSelectionMode = true
                        notifyDataSetChanged()
                        onSelectionModeChange.onSelectionModeChange(isInSelectionMode)
                    }
                }
                true
            }
        }

        fun bind(photo: SharedStoragePhoto, isInSelectionMode: Boolean, position: Int) {
            rbSelect.visibility = if (isInSelectionMode) View.VISIBLE else View.GONE
            favoriteImages = sharedPreferenceManager.getFavoriteImages().map { Uri.parse(it).toString() }.toMutableList()
            if (isInSelectionMode && !isFavouriteTab) {
                ivImageFavorite.visibility = View.VISIBLE
                if (favoriteImages.map { it.toString() }.contains(photo.contentUri.toString())) {
                    ivImageFavorite.setColorFilter(Color.RED)
                } else {
                    ivImageFavorite.clearColorFilter()
                }
            } else {
                ivImageFavorite.visibility = View.GONE
            }
            rbSelect.isChecked = selectedPositions.contains(position)
            if (!isInSelectionMode) {
                selectedPositions.clear()
            }
            val aspectRatio = photo.width.toFloat() / photo.height.toFloat()
            ConstraintSet().apply {
                clone(binding.root)
                setDimensionRatio(binding.ivPhoto.id, aspectRatio.toString())
                applyTo(binding.root)
            }

            // Load the gray placeholder first
            Glide.with(binding.ivPhoto)
                .load(ColorDrawable(Color.GRAY))
                .into(binding.ivPhoto)

            // Load the actual image only if the item is visible on the screen
            visibleItemPositions.forEach {
                Glide.with(binding.ivPhoto)
                    .load(photo.contentUri)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(ColorDrawable(Color.GRAY))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ) = false.also { Log.e("Glide", "Load failed", e) }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: com.bumptech.glide.load.DataSource?,
                            isFirstResource: Boolean
                        ) = false.also { onHideLoadingDialog() }
                    })
                    .into(binding.ivPhoto)
            }
        }
    }

    override fun getItemId(position: Int) = getItem(position).id.hashCode().toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val binding = ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val parent = holder.itemView.parent
        if (parent is RecyclerView) {
            val layoutManager = parent.layoutManager as StaggeredGridLayoutManager
            val firstVisibleItemPositions = IntArray(layoutManager.spanCount)
            val lastVisibleItemPositions = IntArray(layoutManager.spanCount)
            layoutManager.findFirstVisibleItemPositions(firstVisibleItemPositions)
            layoutManager.findLastVisibleItemPositions(lastVisibleItemPositions)
            // Combine the arrays
            visibleItemPositions = firstVisibleItemPositions + lastVisibleItemPositions
        }
        holder.bind(getItem(position), isInSelectionMode, position)
    }

    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<SharedStoragePhoto>() {
            override fun areItemsTheSame(oldItem: SharedStoragePhoto, newItem: SharedStoragePhoto) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: SharedStoragePhoto, newItem: SharedStoragePhoto) =
                oldItem == newItem
        }
    }
    fun setVisibleItemPositions(positions: IntArray) {
        visibleItemPositions = positions
    }
    fun setLastVisibleItemPosition(position: Int) {
        lastVisibleItemPosition = position
        val array = createArray(lastVisibleItemPosition)
        setVisibleItemPositions(array)
    }
    fun createArray(lastVisibleItemPosition: Int): IntArray {
        val maxElements = 20
        val largestElement = lastVisibleItemPosition + 6
        val startIndex = if (largestElement - maxElements + 1 > 0) largestElement - maxElements + 1 else 1
        return IntArray(maxElements) { startIndex + it }
    }
    fun clearSelectedPositions() {
        selectedPositions.clear()
    }

    fun selectAll() {
        selectedPositions.clear()
        for (i in 0 until itemCount) {
            selectedPositions.add(i)
        }
        notifyDataSetChanged()
    }

    fun updateSelectionMode(isInSelectionMode: Boolean) {
        this.isInSelectionMode = isInSelectionMode
        notifyDataSetChanged()
    }

    fun updateFavouriteTab(isFavouriteTab: Boolean) {
        this.isFavouriteTab = isFavouriteTab
        notifyDataSetChanged()
    }
}