package com.example.app.Adapter.filterAdapter

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.app.R
import com.example.app.data.ImageFilter
import com.example.app.databinding.ItemContainerFilterBinding
import com.example.demoservices.listeners.ImageFilterListener
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ImageFilterAdapter(
    private val imageFilters: List<ImageFilter>,
    private val imageFilterListener: ImageFilterListener
) : RecyclerView.Adapter<ImageFilterAdapter.ImageFilterViewHolder>() {

    private var selectFilterPosition = 0
    private var previouslySelectedFilterPosition = 0

    inner class ImageFilterViewHolder(val binding: ItemContainerFilterBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageFilterViewHolder {
        val binding =
            ItemContainerFilterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageFilterViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return imageFilters.size
    }

    private val executorService: ExecutorService = Executors.newSingleThreadScheduledExecutor()
    private val handler = Handler(Looper.getMainLooper())
    override fun onBindViewHolder(
        holder: ImageFilterViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        with(holder) {
            with(imageFilters[position]) {
                binding.tvTitleFilter.text = name
                // Load mặc định hình ảnh màu xám
                Glide.with(binding.root.context)
                    .load(R.drawable.image_place_holder)
                    .into(binding.imageFilterPriview)

                // Tạo ra thread để load dần các item lên
                handler.postDelayed({
                    executorService.submit {
                        generateFilterPreview(
                            binding,
                            filterPreview,
                            saturation,
                            brightness,
                            warmth,
                            contrast
                        )
                        isLoading = false
                    }
                }, 100L) // Tăng thời gian delay dựa trên vị trí để tạo hiệu ứng load dần

                binding.root.setOnClickListener {
                    if (position != selectFilterPosition) {
                        imageFilterListener.onFilterSelect(this)
                        previouslySelectedFilterPosition = selectFilterPosition
                        selectFilterPosition = position
                        with(this@ImageFilterAdapter) {
                            notifyItemChanged(previouslySelectedFilterPosition, Unit)
                            notifyItemChanged(selectFilterPosition, Unit)
                        }
                    }
                }
            }
            binding.tvTitleFilter.setTextColor(
                ContextCompat.getColor(
                    binding.root.context,
                    if (selectFilterPosition == position) android.R.color.holo_blue_dark else android.R.color.black
                )
            )
        }
    }

    private fun generateFilterPreview(
        binding: ItemContainerFilterBinding,
        filterPreview: Bitmap,
        saturation: Float,
        brightness: Float,
        warmth: Float,
        contrast: Float
    ) {
        // Apply filters and update the view on the main thread
        Handler(Looper.getMainLooper()).post {
            Glide.with(binding.root.context)
                .load(filterPreview)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imageFilterPriview)

            binding.imageFilterPriview.saturation = saturation
            binding.imageFilterPriview.brightness = brightness
            binding.imageFilterPriview.warmth = warmth
            binding.imageFilterPriview.contrast = contrast
            binding.progressBarFilterPreview.visibility = View.GONE
        }
    }
}