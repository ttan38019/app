package com.example.app.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.app.Activity.DetailImageActivity
import com.example.app.Activity.DocumentViewerActivity
import com.example.app.Fragment.DocumentPickerFragment
import com.example.app.R

class DocumentAdapter(
    private val context: Context,
    private val documentUris: MutableList<Uri>, // Change to MutableList
    private val fragment: Fragment // Add fragment reference
) : ArrayAdapter<Uri>(context, 0, documentUris) {

    private class ViewHolder {
        var documentName: TextView? = null
        var imageView: ImageView? = null
        var buttonDelete: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val viewHolder: ViewHolder
        var itemView = convertView

        if (itemView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.item_document, parent, false)
            viewHolder = ViewHolder().apply {
                documentName = itemView.findViewById(R.id.documentName)
                imageView = itemView.findViewById(R.id.imageView2)
                buttonDelete = itemView.findViewById(R.id.delete_icon)
                buttonDelete?.visibility = View.GONE
            }
            itemView.tag = viewHolder
        } else {
            viewHolder = itemView.tag as ViewHolder
        }

        val documentUri = getItem(position)
        Log.e("DocumentAdapter", "Uri: $documentUri")

        viewHolder.buttonDelete?.setOnClickListener {
            documentUris.removeAt(position)
            notifyDataSetChanged()
            if (fragment is DocumentPickerFragment) {
                fragment.removeDocumentUri(documentUri!!)
            }
        }

        itemView?.setOnLongClickListener {
            viewHolder.buttonDelete?.visibility = if (viewHolder.buttonDelete?.visibility == View.GONE) View.VISIBLE else View.GONE
            true
        }

        //xử lý ảnh hiển thị ở listview
        documentUri?.let { uri ->
            val mimeType = context.contentResolver.getType(uri)
            viewHolder.documentName?.text = getFileName(uri)
            Log.e("DocumentAdapter", "MIME type: $mimeType")

            when {
                //image
                mimeType?.startsWith("image/") == true -> {
                    Glide.with(context).load(uri).into(viewHolder.imageView!!)
                    itemView?.setOnClickListener {
                        val intent = Intent(context, DetailImageActivity::class.java).apply {
                            putExtra("imageUri", uri.toString())
                        }
                        context.startActivity(intent)
                    }
                }

                //excel
                mimeType == "application/vnd.ms-excel" || mimeType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" -> {
                    viewHolder.imageView?.setImageResource(R.drawable.ic_document)
                    itemView?.setOnClickListener {
                        val intent = Intent(context, DocumentViewerActivity::class.java).apply {
                            data = uri
                        }
                        context.startActivity(intent)
                    }
                }

                //word
                mimeType == "application/msword" || mimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" -> {
                    viewHolder.imageView?.setImageResource(R.drawable.ic_word)
                    itemView?.setOnClickListener {
                        val intent = Intent(context, DocumentViewerActivity::class.java).apply {
                            data = uri
                        }
                        context.startActivity(intent)
                    }
                }

                //pdf
                mimeType == "application/pdf" -> {
                    viewHolder.imageView?.setImageResource(R.drawable.icon_pdf)
                    itemView?.setOnClickListener {
                        val intent = Intent(context, DocumentViewerActivity::class.java).apply {
                            data = uri
                        }
                        context.startActivity(intent)
                    }
                }
                else -> {
                    viewHolder.imageView?.setImageResource(R.drawable.icon_file) // Set generic file icon
                }
            }
        } ?: run {
            viewHolder.documentName?.text = "Unknown Document"
            viewHolder.imageView?.setImageResource(R.drawable.icon_file) // Set generic file icon
        }

        return itemView!!
    }

    @SuppressLint("Range")
    private fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            context.contentResolver.query(uri, null, null, null, null)?.use { cursor ->
                if (cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result?.lastIndexOf('/') ?: -1
            if (cut != -1) {
                result = result?.substring(cut + 1)
            }
        }
        return result ?: ""
    }
}

