package com.example.app

import android.app.Application
import com.example.app.dependencyinjection.repositoryModule
import com.example.demoservices.dependencyinjection.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyApplication)
            modules(listOf(repositoryModule, viewModelModule))
        }
    }
}