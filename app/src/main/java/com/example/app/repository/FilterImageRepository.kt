package com.example.demoservices.repository

import android.graphics.Bitmap
import android.net.Uri
import com.example.app.data.ImageFilter

interface FilterImageRepository {
    suspend fun prepareImagePreview(imageUri: Uri): Bitmap?
    suspend fun getImageFilters(image: Bitmap): List<ImageFilter>
}
