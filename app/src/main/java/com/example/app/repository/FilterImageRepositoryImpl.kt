package com.example.demoservices.repository

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Paint
import android.net.Uri
import com.example.app.data.ImageFilter
import java.io.InputStream

class FilterImageRepositoryImpl(private val context: Context) : FilterImageRepository {

    override suspend fun prepareImagePreview(imageUri: Uri): Bitmap? {
        getInputStreamFromUri(imageUri)?.use { inputStream ->
            val originalBitmap = BitmapFactory.decodeStream(inputStream)
            val width = context.resources.displayMetrics.widthPixels
            val height = (originalBitmap.height.toFloat() / originalBitmap.width.toFloat() * width)
            return Bitmap.createScaledBitmap(originalBitmap, width, height.toInt(), false)
        } ?: return null
    }

    override suspend fun getImageFilters(image: Bitmap): List<ImageFilter> {
        val imageFilters: ArrayList<ImageFilter> = arrayListOf()

        // Original filter
        imageFilters.add(
            ImageFilter(
                name = "Original",
                saturation = 1.0f,
                brightness = 1.0f,
                warmth = 1.0f,
                contrast = 1.0f,
                filterPreview = image
            )
        )

        // Sepia filter
        imageFilters.add(
            ImageFilter(
                name = "Sepia",
                saturation = 0.5f,
                brightness = 1.0f,
                warmth = 1.5f,
                contrast = 1.0f,
                filterPreview = applyFilter(image, 0.5f, 1.0f, 1.5f, 1.0f)
            )
        )

        // High Contrast filter
        imageFilters.add(
            ImageFilter(
                name = "High Contrast",
                saturation = 1.0f,
                brightness = 1.0f,
                warmth = 1.0f,
                contrast = 2.0f,
                filterPreview = applyFilter(image, 1.0f, 1.0f, 1.0f, 2.0f)
            )
        )

        // Grayscale filter
        imageFilters.add(
            ImageFilter(
                name = "Grayscale",
                saturation = 0.0f,
                brightness = 1.0f,
                warmth = 1.0f,
                contrast = 1.0f,
                filterPreview = applyFilter(image, 0.0f, 1.0f, 1.0f, 1.0f)
            )
        )

        // Warm filter
        imageFilters.add(
            ImageFilter(
                name = "Warm",
                saturation = 1.0f,
                brightness = 1.0f,
                warmth = 2.0f,
                contrast = 1.0f,
                filterPreview = applyFilter(image, 1.0f, 1.0f, 2.0f, 1.0f)
            )
        )

        // Cool filter
        imageFilters.add(
            ImageFilter(
                name = "Cool",
                saturation = 1.0f,
                brightness = 1.0f,
                warmth = 0.5f,
                contrast = 1.0f,
                filterPreview = applyFilter(image, 1.0f, 1.0f, 0.5f, 1.0f)
            )
        )

        imageFilters.add(
            ImageFilter(
                name = "Vintage",
                saturation = 0.8f,
                brightness = 0.9f,
                warmth = 1.3f,
                contrast = 1.1f,
                filterPreview = applyFilter(image, 0.8f, 0.9f, 1.3f, 1.1f)
            )
        )

        // Bright filter
        imageFilters.add(
            ImageFilter(
                name = "Bright",
                saturation = 1.2f,
                brightness = 1.3f,
                warmth = 1.0f,
                contrast = 1.0f,
                filterPreview = applyFilter(image, 1.2f, 1.3f, 1.0f, 1.0f)
            )
        )

        // Muted filter
        imageFilters.add(
            ImageFilter(
                name = "Muted",
                saturation = 0.5f,
                brightness = 0.8f,
                warmth = 1.0f,
                contrast = 0.8f,
                filterPreview = applyFilter(image, 0.5f, 0.8f, 1.0f, 0.8f)
            )
        )

        // Shadow filter
        imageFilters.add(
            ImageFilter(
                name = "Shadow",
                saturation = 1.0f,
                brightness = 0.7f,
                warmth = 1.0f,
                contrast = 1.2f,
                filterPreview = applyFilter(image, 1.0f, 0.7f, 1.0f, 1.2f)
            )
        )

        // High Saturation filter
        imageFilters.add(
            ImageFilter(
                name = "High Saturation",
                saturation = 2.0f,
                brightness = 1.0f,
                warmth = 1.0f,
                contrast = 1.0f,
                filterPreview = applyFilter(image, 2.0f, 1.0f, 1.0f, 1.0f)
            )
        )

        // Low Saturation filter
        imageFilters.add(
            ImageFilter(
                name = "Low Saturation",
                saturation = 0.3f,
                brightness = 1.0f,
                warmth = 1.0f,
                contrast = 1.0f,
                filterPreview = applyFilter(image, 0.3f, 1.0f, 1.0f, 1.0f)
            )
        )

        // Soft filter
        imageFilters.add(
            ImageFilter(
                name = "Soft",
                saturation = 1.0f,
                brightness = 1.1f,
                warmth = 1.0f,
                contrast = 0.9f,
                filterPreview = applyFilter(image, 1.0f, 1.1f, 1.0f, 0.9f)
            )
        )

        // Sharp filter
        imageFilters.add(
            ImageFilter(
                name = "Sharp",
                saturation = 1.0f,
                brightness = 1.0f,
                warmth = 1.0f,
                contrast = 1.5f,
                filterPreview = applyFilter(image, 1.0f, 1.0f, 1.0f, 1.5f)
            )
        )

        // Add more filters as needed

        return imageFilters
    }

    private fun applyFilter(
        image: Bitmap,
        saturation: Float,
        brightness: Float,
        warmth: Float,
        contrast: Float
    ): Bitmap {
        // Create a mutable copy of the bitmap to apply the filters
        val filteredBitmap = image.copy(image.config, true)

//        // Create a new color matrix
//        val colorMatrix = ColorMatrix()
//
//        // Apply the saturation adjustment
//        colorMatrix.setSaturation(saturation)
//
//        // Apply the brightness adjustment
//        colorMatrix.postConcat(ColorMatrix(floatArrayOf(
//            1f, 0f, 0f, 0f, brightness,
//            0f, 1f, 0f, 0f, brightness,
//            0f, 0f, 1f, 0f, brightness,
//            0f, 0f, 0f, 1f, 0f
//        )))
//
//        // Apply the warmth adjustment
//        // This is a simple implementation of warmth adjustment, you may need a more complex one for better results
//        colorMatrix.postConcat(ColorMatrix(floatArrayOf(
////            1f, 0f, 0f, 0f, warmth,
////            0f, 1f, 0f, 0f, 0f,
//            0f, 0f, 1f, 0f, -warmth,
//            0f, 0f, 0f, 1f, 0f
//        )))
//
//        // Apply the contrast adjustment
//        val scale = contrast + 1f
//        val translate = (-.5f * scale + .5f) * 255f
//        colorMatrix.postConcat(ColorMatrix(floatArrayOf(
//            scale, 0f, 0f, 0f, translate,
//            0f, scale, 0f, 0f, translate,
//            0f, 0f, scale, 0f, translate,
//            0f, 0f, 0f, 1f, 0f
//        )))
//
//        // Apply the color matrix to the bitmap
//        val paint = Paint()
//        paint.colorFilter = ColorMatrixColorFilter(colorMatrix)
//        val canvas = Canvas(filteredBitmap)
//        canvas.drawBitmap(filteredBitmap, 0f, 0f, paint)
        return filteredBitmap
    }

    private fun getInputStreamFromUri(imageUri: Uri): InputStream? {
        return context.contentResolver.openInputStream(imageUri)
    }
}
